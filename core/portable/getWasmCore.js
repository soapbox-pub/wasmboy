// JS Implementation of Instantiating the Wasm Build

// Import our web assembly module
import wasmModuleUrl from '../../dist/core/core.untouched.wasm';

// Import our wasm import object
import importObject from './importObject';

const readBase64Buffer = base64String => {
  const base64Encoded = base64String.split(',')[1];
  const binaryString = atob(base64Encoded);
  const len = binaryString.length;
  const bytes = new Uint8Array(len);

  for (let i = 0; i < len; i++) {
    bytes[i] = binaryString.charCodeAt(i);
  }

  return bytes;
};

const wasmNodeInstantiate = async wasmModuleUrl => {
  const wasmBuffer = readBase64Buffer(wasmModuleUrl);
  return await WebAssembly.instantiate(wasmBuffer, importObject);
};

// Function to instantiate our wasm and respond back
const getWasmBoyWasmCore = async () => {
  const response = await wasmNodeInstantiate(wasmModuleUrl);

  // Set our wasmInstance and byte memory in the main thread
  const instance = response.instance;
  const byteMemory = new Uint8Array(instance.exports.memory.buffer);
  return {
    instance,
    byteMemory,
    type: 'Web Assembly'
  };
};

export default getWasmBoyWasmCore;
